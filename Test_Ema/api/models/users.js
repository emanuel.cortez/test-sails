module.exports = {
  migrate: 'safe',
  connection: 'mysql_db',
  attributes: {
    id: {
      type: 'integer',
      unique: true,
      primaryKey: true,
      autoIncrement: false,
    },
    firstName: {
      type: 'string'
    },
    lastName: {
      type: 'string'
    },

    // Add a reference to Pet
    pets: {
      collection: 'pet',
      via: 'owners'
    }
  }
};

// curl -X PATCH http://localhost:1337/users/update/1 -H "Content-type: application/json" -d @/home/emanuelcm/permisos.json -v